/**
 * @version 1.0.0.0
 * @copyright Copyright ©  2018
 * @compiler Bridge.NET 17.3.0
 */
Bridge.assembly("PedidosRedondos", function ($asm, globals) {
    "use strict";

    Bridge.define("PedidosRedondos.Class1", {
        main: function Main () {
            var $t;
            var button = ($t = document.createElement("button"), $t.innerHTML = "Click Me", $t.onclick = function (ev) {
                System.Console.WriteLine("Success!");
                window.alert("Success!");
            }, $t);
            document.body.appendChild(button);
        }
    });
});

//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAiZmlsZSI6ICJQZWRpZG9zUmVkb25kb3MuanMiLAogICJzb3VyY2VSb290IjogIiIsCiAgInNvdXJjZXMiOiBbIkNsYXNzMS5jcyJdLAogICJuYW1lcyI6IFsiIl0sCiAgIm1hcHBpbmdzIjogIjs7Ozs7Ozs7Ozs7WUFZWUEsYUFBYUEsZ0ZBR0NBLFVBQUNBO2dCQUlQQTtnQkFDQUE7O1lBSVJBLDBCQUEwQkEiLAogICJzb3VyY2VzQ29udGVudCI6IFsidXNpbmcgU3lzdGVtO1xyXG51c2luZyBCcmlkZ2U7XHJcbnVzaW5nIEJyaWRnZS5IdG1sNTtcclxudXNpbmcgUmV0eXBlZDtcclxuXHJcbm5hbWVzcGFjZSBQZWRpZG9zUmVkb25kb3Ncclxue1xyXG4gICAgcHVibGljIGNsYXNzIENsYXNzMVxyXG4gICAge1xyXG4gICAgICAgIHB1YmxpYyBzdGF0aWMgdm9pZCBNYWluKClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vIENyZWF0ZSBhIG5ldyBCdXR0b25cclxuICAgICAgICAgICAgdmFyIGJ1dHRvbiA9IG5ldyBIVE1MQnV0dG9uRWxlbWVudFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBJbm5lckhUTUwgPSBcIkNsaWNrIE1lXCIsXHJcbiAgICAgICAgICAgICAgICBPbkNsaWNrID0gKGV2KSA9PlxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFdoZW4gQnV0dG9uIGlzIGNsaWNrZWQsIFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoZSBCcmlkZ2UgQ29uc29sZSBzaG91bGQgb3Blbi5cclxuICAgICAgICAgICAgICAgICAgICBDb25zb2xlLldyaXRlTGluZShcIlN1Y2Nlc3MhXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIFdpbmRvdy5BbGVydChcIlN1Y2Nlc3MhXCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAvLyBBZGQgdGhlIEJ1dHRvbiB0byB0aGUgcGFnZVxyXG4gICAgICAgICAgICBEb2N1bWVudC5Cb2R5LkFwcGVuZENoaWxkKGJ1dHRvbik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdCn0K
