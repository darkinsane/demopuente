﻿using System;
using Bridge;
using Bridge.Html5;
using Retyped;

namespace PedidosRedondos
{
    public class Class1
    {
        public static void Main()
        {
            // Create a new Button
            var button = new HTMLButtonElement
            {
                InnerHTML = "Click Me",
                OnClick = (ev) =>
                {
                    // When Button is clicked, 
                    // the Bridge Console should open.
                    Console.WriteLine("Success!");
                    Window.Alert("Success!");
                }
            };
            // Add the Button to the page
            Document.Body.AppendChild(button);
        }
    }
}
